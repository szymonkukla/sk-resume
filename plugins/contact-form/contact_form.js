$(document).ready(function() {
    var form = $('#contact-form');
    form.append('<div id="message-panel" class="">'
					+ '<div id="returnmessage" class="panel-heading text-left">'
					+ '</div></div>');
    form.append('<div class="form-group">'
                    + '<input type="text" class="form-control" id="name" placeholder="Imię"></div>'
                    + '<div class="form-group">'
                    + '<input type="text" class="form-control" id="email" placeholder="Email"></div>');
    form.append('<div class="form-group">'
                    + '<textarea class="form-control" id="message" rows="8" placeholder="Wiadomość"></textarea></div>');
    form.append('<input type="button" id="submit" class="btn-block" value="Wyślij">');
    $('#message-panel').hide();
    $("#submit").click(function() {
        var name = $("#name").val();
        var email = $("#email").val();
        var message = $("#message").val();
        if (name == '' || email == '') {
            alert("Please Fill Required Fields");
        } else {
            // Returns successful data submission message when the entered information is stored in database.
            $.post("plugins/contact-form/contact_form.php", {
                name1: name,
                email1: email,
                message1: message,
            }, function(data) {
                    if (data == 'true') {
                        $("#message-panel").show();
                        $("#message-panel").removeClass();
                        $("#message-panel").addClass('panel panel-success');
                        $("#returnmessage").html('<i class="fa fa-check-square"></i>&nbsp;Wiadomość została wysłana pomyślnie!');
                    } else {
                        $("#message-panel").show();
                        $("#message-panel").removeClass();
                        $("#message-panel").addClass('panel panel-danger');
                        $("#returnmessage").html('<i class="fa fa-exclamation-circle"></i>&nbsp;Błąd wysyłania! Sprawdź adres email!');
                    };
            });
        };
    });
});